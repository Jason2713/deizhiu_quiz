import React, {Component} from "react"; //{} means optional import. Component was not in the code base
import { getQuestionOfType } from './QuestionGetter';
import TEXT_TYPE from './data/TEXT_TYPE';
// import ModeArea from './mode_area/ModeArea';
import QuizArea from './quiz_area/QuizArea';
import PopupMenu from './popup/Menu';

import './App.css';

class App extends Component{ //App is a subclass of Component class
    state = {
        textTypeRequest: TEXT_TYPE.RANDOM,
        currQuestion: null,
        userCurrentChoice: null, //null, MC1, MC2, MC3
        userSubmission: null, //null, MC1, MC2, MC3
        answered: false,
        answerIsCorrect: false,
        hintDisplay: false,
        chapterNo: 1,
        showPopup: false
    };
    componentDidMount(){
        this.handleNextQuestion();
    }
    handleNextQuestion = ()=>{
        //fetch a question
        const textTypeReq = this.state.textTypeRequest;
        const itemTypeReq = this.state.itemTypeRequest;
        const currQuestion = getQuestionOfType(textTypeReq, itemTypeReq, this.state.chapterNo);

        //update state
        this.setState({currQuestion, answered: false, userSubmission: "", hintDisplay: false});

        // auto focus to the title
        const title = document.getElementById("title");
        if (!title) return;
        title.focus();
    }
    handleHintDisplay = ()=>{
        this.setState({hintDisplay: true});
    }
    updateTheme = (event)=>{
        console.log(event.target.id);
        this.setState({userCurrentChoice: event.target.id});
    }
    updateCurrentChoice = (event)=>{
        console.log(event.target.id);
        this.setState({userCurrentChoice: event.target.id});
    }
    handleSubmission = ()=>{
        // console.log("submission = ", this.state.userCurrentChoice);
        const userSubmission = this.state.userCurrentChoice;
        const correct_answer = this.state.currQuestion.question;
        if (userSubmission === null){
            console.error('No answer selected');
            return;
        }

        var correct = false;
        if (userSubmission === correct_answer){
            correct = true;
        }
        this.setState({userSubmission, answerIsCorrect: correct, answered: true, hintDisplay: false});

        // hide mobile phone's virtual keyboard
        var field = document.createElement('input');
        field.setAttribute('type', 'text');
        document.body.appendChild(field);

        setTimeout(function() {
            field.focus();
            setTimeout(function() {
                field.setAttribute('style', 'display:none;');
            }, 50);
        }, 50);
    }
    togglePopup = ()=>{
        this.setState({  
            showPopup: !this.state.showPopup
        }); 
    }

    handleModeReset = ()=>{
        this.setState({
            textTypeRequest: TEXT_TYPE.RANDOM, 
            quizNo: 1
        }, ()=>{this.handleNextQuestion()});
    }
    setTextTypeReq = (textTypeRequest)=>{
        if (textTypeRequest === this.state.textTypeRequest) return;
        
        console.log('set requested text type to', textTypeRequest);
        this.setState({textTypeRequest}, ()=>{this.handleNextQuestion()});
    }
    setChapterNo = (chapterNo)=>{
        if (chapterNo === this.state.chapterNo) return;

        console.log('set requested quiz type to', chapterNo);
        this.setState({chapterNo}, ()=>{this.handleNextQuestion()});
    }
    render(){
        if (!this.state.currQuestion){
            return null;
        }
        return(
            <div className='App'>
                {/* Fixed buttons */}
                <button id='menu' className='fixed-button' onClick={ this.togglePopup }> = </button>

                <button id='default'  className='fixed-button' onClick={ this.updateTheme }></button>
                <button id='tropical' className='fixed-button' onClick={ this.updateTheme }></button>
                <button id='water'    className='fixed-button' onClick={ this.updateTheme }></button>

                <div style={{margin: '80px 0 40px 0'}}>
                    <h1 id='title'>練習潮洲語！</h1>
                </div>
            
                {
                    this.state.showPopup?
                    <PopupMenu closePopup={ this.togglePopup }>
                    </PopupMenu>
                    :null
                }

                <QuizArea
                    handleKeypress={ this.handleKeypress }
                    updateSubmission={ this.updateSubmission }
                    handleSubmission={ this.handleSubmission }
                    handleNextQuestion={ this.handleNextQuestion }
                    handleHintDisplay={ this.handleHintDisplay }
                    updateCurrentChoice={ this.updateCurrentChoice }

                    qMode={ this.state.qMode }
                    currQuestion={ this.state.currQuestion }
                    hintDisplay={ this.state.hintDisplay }
                    answered={ this.state.answered }
                    answerIsCorrect={ this.state.answerIsCorrect }
                    currChoice={ this.state.userCurrentChoice }
                    userSubmission={ this.state.userSubmission }
                />
            </div>
        )
    }
}
export default App;