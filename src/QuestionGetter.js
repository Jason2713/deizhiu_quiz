import TEXT_TYPE from './data/TEXT_TYPE';
import Chapter1_2 from './data/chapter1_2';


function getRandQuestion(quizNo){
    var Chapter = null;
    switch (quizNo){
        case 1: Chapter = Chapter1_2; break;
        default: Chapter = Chapter1_2;
    }
    const q_size = Object.keys(Chapter).length;
    const q_no = Math.floor(Math.random() * q_size);

    // console.log(Quiz1[q_no]);
    return Chapter[q_no];
}
export function getQuestionOfType(textTypeReq, quizNo){
    // console.log('at getQuestionOfType', textTypeReq, quizNo);

    //trap
    if (Object.keys(TEXT_TYPE).length < textTypeReq){
        console.error('invalid request for question text type:', textTypeReq);
        return {};
    }

    var randQuestion;
    var txtType;
    var textTypeReqOK = false;
    var i=0;
    do{
        //get a random question
        randQuestion = getRandQuestion(quizNo);
        
        txtType = randQuestion.textType;
        textTypeReqOK = (textTypeReq === TEXT_TYPE.RANDOM || textTypeReq === txtType);

        //safety net
        if (i > 10000){
            console.error('too many iterations during question request');
            return {};
        }
        i += 1;
    }
    while( !(textTypeReqOK) );

    const finalQuestion = randQuestion;
    return finalQuestion;
}