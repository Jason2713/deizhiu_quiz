import React from 'react';
import clsx from 'clsx';


const ModeButton = (props)=>{
    const handleButtonClick = (buttonType)=>{
        props.setReqTo(buttonType);
    }
    const { buttonType, selectedButtonType, buttonClass } = props;
    // console.log(buttonClass);
    return(
        <button className={clsx([buttonClass], {'selected': selectedButtonType===buttonType})} onClick={ ()=>handleButtonClick(buttonType) }> 
            { props.children }
        </button>
    )
}
export default ModeButton;