import React from 'react';
import ModeButton from './ModeButton';
import TEXT_TYPE from '../data/TEXT_TYPE';
import '../App.css';
import './ModeArea.css';

const ModeArea = (props)=>{
    //functions
    const { setTextTypeReq, toggleQuestionMode } = props;
    //variables
    const { textTypeRequest } = props;
    return(
        <div className='App-header'>
            <em className='fade-in'>Choose your flavours</em>
            <div className='mode-buttons'>
                
                <div className='flex-box fade-in'>
                    <button id='q-mode' onClick={ toggleQuestionMode }> Eng / Jap </button>
                </div>
                
                <div className='flex-box fade-in'>
                    <ModeButton setReqTo={setTextTypeReq} buttonType={TEXT_TYPE.HIRAGANA} selectedButtonType={textTypeRequest} buttonClass='text-type'> 
                        Hiragana 
                    </ModeButton>
                    <ModeButton setReqTo={setTextTypeReq} buttonType={TEXT_TYPE.KATAKANA} selectedButtonType={textTypeRequest} buttonClass='text-type'> 
                        Katakana 
                    </ModeButton>
                    <ModeButton setReqTo={setTextTypeReq} buttonType={TEXT_TYPE.RANDOM} selectedButtonType={textTypeRequest} buttonClass='text-type'> 
                        Random 
                    </ModeButton>
                </div>
            </div>
        </div>
    )
}
export default ModeArea;