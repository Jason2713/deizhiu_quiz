import React from 'react';
import './quiz_area.css';
import clsx from 'clsx';

const QuizArea = (props) => {
    //functions
    const { handleSubmission, handleNextQuestion, handleHintDisplay, updateCurrentChoice } = props;
    //variables
    const { currQuestion, hintDisplay, answered, answerIsCorrect, userSubmission, currChoice } = props;
    return (
        <div>
            <div>
                <div>
                    <h3> 以下的潮洲語是？ </h3>
                    <button style={{borderRadius: '50%'}}>
                        <i className='material-icons'>volume_down</i>
                    </button>
                </div>
                <div style={{margin: '20px 0', height: '10vh'}}>
                    <button
                        id='MC1'
                        className={clsx('MC-button', {'chosen': currChoice==='MC1'})}
                        onClick={ updateCurrentChoice }>
                            MC 1
                    </button>
                    <button 
                        id='MC2' 
                        className={clsx('MC-button', {'chosen': currChoice==='MC2'})}
                        onClick={ updateCurrentChoice }>
                            MC 2
                    </button>
                    <button 
                        id='MC3' 
                        className={clsx('MC-button', {'chosen': currChoice==='MC3'})}
                        onClick={ updateCurrentChoice }>
                            MC 3
                    </button>
                </div>
                <div style={{margin: '20px 0'}}>
                    <button onClick={ handleSubmission }> 提交 </button>
                    <button id='skip-button' onClick={ handleNextQuestion }> 跳過！ </button>
                    <button id='hint-button' onClick={ handleHintDisplay }> 提示？ </button> 
                </div>
    
                {
                    hintDisplay?(
                        <h3> 提示: {currQuestion.hint} </h3>
                    ):null
                }
            </div>
    
            {
                // This area shows when an answer is submitted.
                answered?(
                    <div>
                        <br/>
                        {
                            answerIsCorrect?(
                                <h3> Correct! Keep it up! </h3>
                            ):(
                                <h3> Wrong. Better luck next time! </h3>
                            )
                        }
                        <div className='answer-border'>
                            <div style={{display: 'flex', flexDirection: 'column'}}>
                                <p> 你的答案 </p>
                                <p> { userSubmission }</p>
                            </div>
                            <div style={{display: 'flex', flexDirection: 'column'}}>
                                <p>Correct Answer</p>
                                <p> { currQuestion.answer }</p>
                            </div>
                        </div>
                        <div style={{display: 'flex', justifyContent:'flex-end', margin: '0 5%'}}>
                            <button id='next-button' onClick={ handleNextQuestion }> >> </button>
                        </div>
                    </div>
                ): null
            }
        </div>
    )
}
export default QuizArea;