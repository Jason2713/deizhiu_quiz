import React from 'react';
import './menu.css';

class PopupMenu extends React.Component{
    render(){
        return(
            <div className='background'>
                <div className='popup-box'>
                    <div className='popup-content'>
                        {this.props.children}
                    </div>
                    <button style={{float: 'right', borderRadius: '4px'}} onClick={this.props.closePopup}> 完成 </button> 
                </div>
            </div>
        )
    }
}
export default PopupMenu;